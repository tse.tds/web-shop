FROM php:7.3-apache

RUN apt update && \
apt install -y libpng-dev libjpeg-dev libfreetype6-dev libcurl4-gnutls-dev libzip-dev && \
docker-php-ext-install -j$(nproc) mysqli curl mbstring zip && \
docker-php-ext-configure gd --with-jpeg-dir=/usr/lib --with-freetype-dir=/usr/include/freetype2 && \
docker-php-ext-install -j$(nproc) gd

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY docker-node-entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

VOLUME /var/storage

ENTRYPOINT ["/entrypoint.sh"]